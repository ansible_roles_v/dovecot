import os
import pytest
import testinfra.utils.ansible_runner

PACKAGES = ['dovecot']
SERVICES = ['dovecot']

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')

@pytest.mark.parametrize('pkg', PACKAGES)
def test_pkg(host, pkg):
    package_objects = host.package(pkg)
    assert package_objects.is_installed

@pytest.mark.parametrize('svc', SERVICES)
def test_svc(host, svc):
    service_object = host.service(svc)
    assert service_object.is_running
    assert service_object.is_enabled
