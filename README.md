Dovecot: IMAP/POP3 server
=========
Installs Dovecot with default parameters.  
You can add you own templates and config files.  
Remember: predefined config files will be replaced by files from templates with the same filenames.  

Include role
------------
```yaml
- name: dovecot  
  src: https://gitlab.com/ansible_roles_v/dovecot/  
  version: main  
```

Example Playbook
----------------
```yaml
- hosts: servers
  gather_facts: true
  become: true
  roles:
    - dovecot
```